import 'mocha';
import { expect } from 'chai';
import { join } from "path";
import { readConfig } from "../../src/config-reader";
import { Renderer } from "../../src/renderer";

describe('Render component', () => {
    it('Should render component to stream', async () => {
        const pathToConfig = join(__dirname, 'ssr-config.json');
        const componentName = 'TestComponent';
        const config = await readConfig(pathToConfig);
        const renderer = new Renderer(config);
        const stream = await renderer.render(componentName);
        const promise = new Promise((resolve: () => void) => {
            let resString = '';

            stream.on('data', (data: string) => {
                resString += data;
            });

            stream.on('end', () => {
                expect(resString).to.equal('<div data-reactroot=""></div>');
                resolve();
            });
        });

        return promise
    });
});