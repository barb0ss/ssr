import 'mocha';
import { expect } from 'chai';
import { readConfig, Config } from '../../src/config-reader';
import { join } from 'path';

describe('Config reader', () => {
    it('Should return config module', (done: () => void) => {
        const pathToConfig = join(__dirname, 'ssr-config.json');

        readConfig(pathToConfig).then((config: Config) => {
            const componentDescr = config.components['TestComponent'];

            expect(componentDescr.path).to.equal('./test-component');
            done();
        });
    });
});
