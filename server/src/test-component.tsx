import * as React from 'react';

export interface TestComponentProps {
    text?: string;
}

export const TestComponent = (props: TestComponentProps) => {
    return <div>{ props.text }</div>;
};
