import { createServer, Server, IncomingMessage, ServerResponse } from 'http';
import { Renderer } from './renderer';
import { readConfig, Config } from './config-reader';

export interface RenderingServerOptions {
    pathToConfig?: string
}

export class RenderingServer {
    private _server: Promise<Server>;
    private _renderer: Renderer | null = null;
    private _config: Config | null = null;

    public constructor(options: RenderingServerOptions = {}) {
        this._server = this._init(options);
    }

    public async listen(port: number): Promise<void> {
        const server = await this._server;
        server.listen(port);
    }

    private async _init(options: RenderingServerOptions): Promise<Server> {
        this._config = await readConfig(options.pathToConfig);
        this._renderer = new Renderer(this._config);

        return createServer(this._createRequestListener(this._renderer));
    }

    private _createRequestListener = (renderer: Renderer) => {
        return (req: IncomingMessage, res: ServerResponse) => {

            const method = req.method;

            if (method === 'POST') {
                let body: Buffer[] = [];

                req.on('data', (chunk: Buffer) => {
                  body.push(chunk);

                }).on('end', () => {
                    const data = JSON.parse(Buffer.concat(body).toString());

                    res.setHeader('Content-Type', 'text/html');

                    renderer.render(data.component, data.props).then((stream: NodeJS.ReadableStream) => {
                        stream.pipe(res);
                    });
                });
            }
        }
    }
}
