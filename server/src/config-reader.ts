import { resolve, dirname } from 'path';

const defaultPath = './ssr-config.json';

export interface ComponentDescription {
    path: string;
}

export interface Config {
    projectDir: string;
    components: {
        [key: string]: ComponentDescription
    };
}

export async function readConfig(path: string = defaultPath): Promise<Config> {
    const configPath = resolve(path);

    const config = await import(configPath);

    config.projectDir = dirname(configPath);

    return config;
}
