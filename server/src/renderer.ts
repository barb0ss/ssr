import { renderToNodeStream } from 'react-dom/server';
import { createElement } from "react";
import { Config } from './config-reader';
import { resolve } from 'path';

export class Renderer {
    private _config: Config;

    public constructor(config: Config) {
        this._config = config;
    }

    public async render(component: string, props?: any): Promise<NodeJS.ReadableStream> {
        const ctor = await this._getComponent(component);
        const reactElem = createElement(ctor, props);

        return renderToNodeStream(reactElem);
    }

    private async _getComponent(component: string): Promise<any> {
        const module = await import(resolve(this._config.projectDir, this._config.components[component].path));

        return module[component];
    }
}
