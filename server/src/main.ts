// import { createServer, Socket } from 'net';
import { RenderingServer } from './rendering-server';

// const server = createServer((c: Socket) => {
//     // 'connection' listener
//     console.log('client connected');
//     c.on('end', () => {
//         console.log('client disconnected');
//     });
//     c.write('hello\r\n');
//     c.pipe(c);
// });
//
// server.on('error', (err: Error) => {
//     throw err;
// });
//
// server.listen('/tmp/echo.sock', () => {
//     console.log('server bound');
// });

const server = new RenderingServer();

server.listen(9000);